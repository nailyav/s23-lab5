# Lab5 -- Integration testing

## Homework

### BVA

| Parameter        | Possible classes              |
|------------------|-------------------------------|
| type             | budget, luxury, nonsense      |
| plan             | minute, fixed_price, nonsense |
| distance         | <=0, >0, >=1000001            |
| planned_distance | <=0, >0, >=1000001            |
| time             | <=0, >0, >=1000001            |
| planned_time     | <=0, >0, >=1000001            |
| inno_discount    | yes, no, nonsense             |

### Decision table

| type     | plan        | distance        | planned_distance | time            | planned_time    | discount | status  |
|----------|-------------|-----------------|------------------|-----------------|-----------------|----------|---------|
| budget   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| budget   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| budget   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| budget   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| luxury   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| luxury   | minute      | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| luxury   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | yes      | Valid   |
| luxury   | fixed_price | >0, , <=1000000 | >0, , <=1000000  | >0, , <=1000000 | >0, , <=1000000 | no       | Valid   |
| nonsense | any         | any             | any              | any             | any             | any      | Invalid |
| any      | nonsense    | any             | any              | any             | any             | any      | Invalid |
| any      | any         | <=0             | any              | any             | any             | any      | Invalid |
| any      | any         | >1000000        | any              | any             | any             | any      | Invalid |
| any      | any         | any             | <=0              | any             | any             | any      | Invalid |
| any      | any         | any             | >1000000         | any             | any             | any      | Invalid |
| any      | any         | any             | any              | <=0             | any             | any      | Invalid |
| any      | any         | any             | any              | >1000000        | any             | any      | Invalid |
| any      | any         | any             | any              | any             | <=0             | any      | Invalid |
| any      | any         | any             | any              | any             | >1000000        | any      | Invalid |
| any      | any         | any             | any              | any             | any             | nonsense | Invalid |


**Here is InnoCar Specs:** \
Budet car price per minute = 22 \
Luxury car price per minute = 44 \
Fixed price per km = 20 \
Allowed deviations in % = 17 \
Inno discount in % = 5 

### Results
Tests for decision table and boundary values

| type    | plan           | distance  | planned_distance | time           | planned_time   | inno_discount | expected        | actual          | passed |
|---------|----------------|-----------|------------------|----------------|----------------|---------------|-----------------|-----------------|--------|
| budget  | minute         | 112       | 120              | 35             | 40             | yes           | 731.5           | 731.5           | +      |
| budget  | minute         | 112       | 120              | 35             | 40             | no            | 770             | 770             | +      |
| budget  | fixed_price    | 112       | 120              | 35             | 40             | yes           | 2280.0          | 554.16666666666 | -      |
| budget  | fixed_price    | 112       | 120              | 35             | 40             | no            | 2400            | 583.33333333333 | -      |
| luxury  | minute         | 112       | 120              | 35             | 40             | yes           | 1463.0          | 1024.1          | -      |
| luxury  | minute         | 112       | 120              | 35             | 40             | no            | 1540            | 1078            | -      |
| luxury  | fixed_price    | 112       | 120              | 35             | 40             | yes           | 2280.0          | Invalid Request | -      |
| luxury  | fixed_price    | 112       | 120              | 35             | 40             | no            | 2400            | Invalid Request | -      |
| budget  | minute         | 1000000   | 1                | 1              | 1              | yes           | 20.9            | 20.9            | +      |
| budget  | minute         | 1         | 1000000          | 1              | 1              | yes           | 20.9            | 20.9            | +      |
| budget  | minute         | 1         | 1                | 1000000        | 1              | yes           | 20900000.0      | 20900000        | +      |
| budget  | minute         | 1         | 1                | 1              | 1000000        | yes           | 20.9            | 20.9            | +      |
| nonsense| minute         | 1         | 1                | 1              | 1              | yes           | Invalid Request | Invalid Request | +      |
| budget  | nonsense       | 1         | 1                | 1              | 1              | yes           | Invalid Request | Invalid Request | +      |
| budget  | minute         | -10       | 1                | 1              | 1              | yes           | Invalid Request | Invalid Request | +      |
| budget  | minute         | 0         | 1                | 1              | 1              | yes           | Invalid Request | 20.9            | -      |
| budget  | minute         | 1000001   | 1                | 1              | 1              | yes           | Invalid Request | 20.9            | -      |
| budget  | minute         | 1         | -10              | 1              | 1              | yes           | Invalid Request | Invalid Request | +      |
| budget  | minute         | 1         | 0                | 1              | 1              | yes           | Invalid Request | 20.9            | -      |
| budget  | minute         | 1         | 1000001          | 1              | 1              | yes           | Invalid Request | 20.9            | -      |
| budget  | minute         | 1         | 1                | -10            | 1              | yes           | Invalid Request | Invalid Request | +      |
| budget  | minute         | 1         | 1                | 0              | 1              | yes           | Invalid Request | 0               | -      |
| budget  | minute         | 1         | 1                | 1000001        | 1              | yes           | Invalid Request | 20900020.9      | -      |
| budget  | minute         | 1         | 1                | 1              | -10            | yes           | Invalid Request | Invalid Request | +      |
| budget  | minute         | 1         | 1                | 1              | 0              | yes           | Invalid Request | 20.9            | -      |
| budget  | minute         | 1         | 1                | 1              | 1000001        | yes           | Invalid Request | 20.9            | -      |
| budget  | minute         | 1         | 1                | 1              | 1              | nonsense      | Invalid Request | 22              | -      |


### Bugs

1. Invalid requests are sometimes validated wrong
2. Price calculation sometimes gives the wrong results

